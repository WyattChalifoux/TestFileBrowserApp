package main

import (
	"io"
	"os"
	"fmt"
	"log"
	"bufio"
	"strings"
	"unicode"
	"sort"
	"strconv"
)

//The default root directory.
var rootDirectory = "./AppRoot"
//The download directory. In a real live web app, this would be changed along with some other implementation details to utilize HTTPS and the browser's download functions.
//However, for a completely local demo application like this one, it is just another local folder.
var rootDownloads = "./Downloads/"

func main(){

	//Thethe application should always be ready to receive new requests
	for true{

		//Read the user's command until a newline character is received.
		requestReceiver := bufio.NewReader(os.Stdin)
		requestString, err := requestReceiver.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}

		//Split the request by unicode spaces.
		requestFields := strings.FieldsFunc(requestString, unicode.IsSpace)

		//Save the number of fields, since we will be accessing it frequently later on.
		requestFieldsLength := len(requestFields)

		//If the user provided a non-blank input
		if requestFieldsLength > 0 {

			//Use a switch so that we don't need to check the value of requestFields[0] multiple times.
			switch requestFields[0]{
			case "BROWSE":
				requestedResource := rootDirectory + "/"

				destinationPage := 1;//default page

				inputPageSize := 32;//default page size

				//This section deals with cases of varying argument numbers.
				//A switch was not used since in the case of having 2 arguments, the code for 1 would need to be duplicated,
				//and in the case for having 3 arguments, the code for 1 AND 2 would need to be duplicated.
				if requestFieldsLength > 1 {
					requestedResource += requestFields[1]

					//If there are three arguments, the third one represents a page the user wishes to browse to.
					if requestFieldsLength > 2 {
						destinationPage, err = strconv.Atoi(requestFields[2])
						if err != nil {
							log.Fatal(err)
						}
					
						//If there are four arguments, the fourth one represents a custom number of items per page supplied by the user.
						if requestFieldsLength > 3 {
							inputPageSize, err = strconv.Atoi(requestFields[3])
							if err != nil {
								log.Fatal(err)
							}
						}
					}
				}
				//At last show the directory with the given info.
				fmt.Println(showDirectory(requestedResource, destinationPage, inputPageSize))
			case "DOWNLOAD":
				requestedResource := rootDirectory + "/" + requestFields[1]
				fmt.Println(downloadFile(requestedResource))
			case "SETROOT":
				rootDirectory = requestFields[1]
				fmt.Println("Set root directory to " + requestFields[1])
			case "SETDOWNLOAD":
				//Be sure to add the trailing / so that the downloads are not sent to the same folder as the application itself!
				rootDownloads = requestFields[1] + "/"
				fmt.Println("Set root download directory to " + requestFields[1])
			default:
				fmt.Println("Unrecognized Command \"" + requestFields[0] +"\"")
			}
		} else {
			fmt.Println("Empty Command")
		}
	}
	
}

func showDirectory(path string, currentPage int, pageSize int) string {

	//What follows is the library implementation of ReadDir, however I have added custom error messages.	 
	f, err := os.Open(path)
    if err != nil {
		return "File could not be opened"
    }
    files, err := f.Readdir(-1)
    f.Close()
    if err != nil {
		return "File could not be closed"
	}

	//Using GO's sort results in an O(n*log n) complexity. This should be upgraded to Radix sort or Quicksort, time permitting
	sort.SliceStable(files, func(i, j int) bool { return files[i].Name() < files[j].Name() })

	//End ReadDir

	//Begin to form the string used for output.
	//The output will be JSON formatted.
	//Since the same variables are always output, a JSON marshal felt like overkill - it was simpler to write it directly.
	outputString := "";
	outputString += "{"

	//Figure out number of pages. Add one so that if the number of files is less than page size we still have 1 page.
	pageCount := len(files)/pageSize + 1;

	//Now that we know how many pages there are, we can check to see if the user has provided a currentPage within bounds.
	if currentPage > pageCount {
		return "Page does not exist"
	}
	
	//printingLimited decides whether the for loop runs til all files are printed, regardless of page size. This is for the special input of -1.
	printingLimited := true;
	if currentPage == -1 {
		//If the user provided something like BROWSE [Path] [-1], then they want to see all the files on one page.
		printingLimited = false;
		
		//Use the special Page Count 0 to indicate that the user has chosen to display the entire directory.
		outputString +=  "\"Page Count\":0,\"Current Page\":1,\"Contents\":["	

		//set the currentPage to 1 so that the startIndex and printing works normally later on.
		currentPage = 1; 
	} else {
		outputString +=  "\"Page Count\":" + strconv.Itoa(pageCount)
		outputString += ",\"Current Page\":" + strconv.Itoa(currentPage)
		outputString += ",\"Contents\":["		
	}

	//Handling the case of an empty directory is more efficient this way, as we don't need to add checks to both the first line print and the for loop
	if len(files) == 0 {
		outputString += "]}"
		return outputString
	}

	//Calculate the index to start printing from. Note the 1 subtracted from current page.
	startIndex := (currentPage-1)*pageSize

	//Print the first file separately. This is just so that the JSON output of Contents doesn't begin or end with a comma, nothing fancy or tricky.
	outputString += "\"" + files[startIndex].Name() +"\""

	//Iterate from the first index on the given page (plus one, since we printed the first above)
	//When the following condition is met:
	//i is less than the directory length AND we haven't printed out more files than the page size (startIndex+pagesize)
	//Unless printing is unlimited, in that case run until i<len(files) no matter what.
	for i := startIndex+1; (i < len(files)) && ((i < startIndex + pageSize) || !printingLimited); i++ {
		outputString += ",\"" + files[i].Name() +"\""
	}

	//Close off the JSON and return it.
	outputString += "]}"

	return outputString
}

func downloadFile(sourceLocation string) string {

	//Split up the file path
	fileName := strings.Split(sourceLocation, "/")

	//The download should have the same file name, and go in our downloads folder
	downloadPath := rootDownloads + fileName[len(fileName)-1]

	//Name the file with a tmp so it doesn't overwrite whatever is currently there
	newfile, err := os.Create(rootDownloads + fileName[len(fileName)-1] + ".tmp")
	if err != nil {
        log.Fatal(err)
	}
	
	//Open the source file
	file, err := os.Open(sourceLocation)
	if err != nil {
        log.Fatal(err)
	}

	//Copy the file over in pieces, io.Copy does this automatically.
	_,  err = io.Copy(newfile,file)
	if err != nil {
		log.Fatal(err)
	}

	//Close the file when done
	newfile.Close()

	//Rename it to no longer have the tmp extension
	err = os.Rename(downloadPath + ".tmp", downloadPath)
	if err != nil {
		log.Fatal(err)
	}

	//Return a confirmation of the download
	return "Downloaded " + fileName[len(fileName)-1]
}